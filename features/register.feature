Feature: Register into losestudiantes
  As an user I want to register within los estudiantes website

  Scenario Outline: Register failed with wrong inputs

    Given I go to losestudiantes home screen
    When I open the login screen
    And Fill in <name1> and <lastname> and <email> and <program> and <password>
    And I try to register
    Then I expect to see error <error>

    Examples:
      | name1   | lastname | email          | program   | password  | error                  |
      | Pedro   | Perez    | pepe@mail.com  | Historia | 1234       | La contraseña debe     |
      | Pedro   | Perez    |                | Historia | 123456789  | Ingresa tu correo      |
      | Pedro   | Perez    | pepe@mail.com  | Historia |            | Ingresa una contraseña |