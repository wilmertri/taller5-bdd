//Complete siguiendo las instrucciones del taller

var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
    Given('I go to losestudiantes home screen', () => {
        browser.url('/');
        if(browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        browser.waitForVisible('button=Ingresar', 3000);
        browser.click('button=Ingresar');
    });

    When('I fill to login correct', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        browser.waitForVisible('input[name="correo"]', 3000);
        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('fabiantriana1072@gmail.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('1072661319')
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.cajaLogIn');
        cajaLogIn.element('button=Ingresar').click()
    });

    Then('I expect to not be able to login', () => {
        browser.waitForVisible('.aviso.alert.alert-danger', 3000);
    });

    When(/^I fill with (.*) and (.*)$/ , (email, password) => {
        var cajaLogIn = browser.element('.cajaLogIn');

        browser.waitForVisible('input[name="correo"]', 3000);
        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys(email);

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(password)
    });

    When(/^Fill in (.*) and (.*) and (.*) and (.*) and (.*)$/, (name1, lastname, email, programa, pass) => {
        var cajaSignUp = browser.element('.cajaSignUp');

        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys(name1);

        var apellidoInput = cajaSignUp.element('input[name="apellido"]');
        apellidoInput.click();
        apellidoInput.keys(lastname);

        var correoInput = cajaSignUp.element('input[name="correo"]');
        correoInput.click();
        correoInput.keys(email);

        var selectBox = cajaSignUp.element('select[name="idPrograma"]');
        selectBox.selectByVisibleText(programa);

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(pass);

        cajaSignUp.element('input[name="acepta"]').click();
    });

    Then('I expect to see {string}', error => {
        browser.waitForVisible('.aviso.alert.alert-danger', 3000);
        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).to.include(error);
    });



    When(/^I fill with inputs (.*) and (.*) and (.*) and (.*) and (.*)$/ , (name, lastname, email, program, password) => {

        var cajaSignUp = browser.element('.cajaSignUp');

        browser.waitForVisible('input[name="nombre"]', 5000);
        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys(name);

        var apellidoInput = cajaSignUp.element('input[name="apellido"]').click();
        apellidoInput.keys(lastname);

        var correoInput = cajaSignUp.element('input[name="correo"]').click();
        correoInput.keys(email);

        var selectBox = cajaSignUp.element('select[name="idPrograma"]');
        selectBox.selectByVisibleText(program);

        var passwordInput = cajaSignUp.element('input[name="password"]').click();
        passwordInput.keys(password);

        cajaSignUp.element('input[name="acepta"]').click();
    });

    When('I try to register', () => {
        var cajaSignUp = browser.element('.cajaSignUp');
        cajaSignUp.element('button=Registrarse').click();
    });

    Then('I expect to see error {string}', error => {
        browser.waitForVisible('.aviso.alert.alert-danger', 3000);
        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).to.include(error);
    });
});